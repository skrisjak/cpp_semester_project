#include <vector>
#include <iostream>
#include <chrono>
#include <thread>
#include "Player.cpp"

#define ANSI_CLEAR  "\033c"
#define RED "\x1B[91m"
#define GREEN "\x1B[92m"
#define BLUE "\x1B[96m"


class Game {
public:
    std::vector<Player> players;
    std::pair<size_t , size_t> currentMove;
    int dices;
    int beginning;
    int current;
    /**
     * pro zkontrolování, jestli byl odhad správný
     * @return true/false
     */

    bool checkDecks() {
        int frequency = 0;
        int guessed = currentMove.second;
        for (Player p: players) {
            for (int d: p.getCup()) {
                if (d == guessed) {
                    frequency++;
                }
            }
        }
        return frequency == currentMove.first;
    }
    /**
     * posune index na dalšího hráče
     */
    void moveCurrent() {
        current++;
        if (current >= players.size()) {
            current = 0;
        }
    }

    /**
     * posune index začínajícího hráče
     */
    void moveBeginning() {
        beginning++;
        if (beginning >= players.size()) {
            beginning = 0;
        }
    }
    /**
     * odstraní hráče ze hry, který nemá kostky
     */
    void eraseDead() {
        auto it = players.begin();
        while (it != players.end()) {
            if (!it->canPlay()) {
                players.erase(it);
                return;
            }
            it++;
        }
    }
    /**
     * sebere kostky hráči, který zavolal bluff/believe
     */
    void punishCurrentPlayer() {
        players.at(current).decreaseSize();
    }

    /**
     * sebere kostky hráči, na kterého byl zavolán bluff/believe
     */
    void punishPreviousPlayer() {
        int previous = current - 1;
        if (previous < 0) {
            previous = players.size() - 1;
        }
        players.at(previous).decreaseSize();
    }

    explicit Game(int gamers) {
        Player player("User", 'u', false);
        players.push_back(player);
        for (int i = 1; i <= gamers; i++) {
            std::string name = "Player ";
            name.append(std::to_string(i));
            char a;
            if (i % 2 == 0) {
                a = 'b';
            } else {
                a = 's';
            }
            Player p(name, a, true);
            players.push_back(p);
        }
        beginning = 0;
        current = 0;
        auto p = std::make_pair(1, 1);
        currentMove = p;
        dices = players.size() * 5;
    }

    /**
     * spustí herní smyčku
     */
    void start() {
        std::cout << "To start a game, press ENTER" <<std::flush;
        std::cin.get();
        //hra probíhá, pokud má dva a více hráčů
        while (players.size() > 1) {
            std::cout << ANSI_CLEAR << std::flush;
            current = beginning;
            std::cout << "Players throw dices" <<std::flush;
            for (int i = 0; i < 4; ++i) {
                std::this_thread::sleep_for(std::chrono::seconds(1));
                std::cout << '.' <<std::flush;
            }
            for (Player &p: players) {
                p.throwDices();
            }
            //počáteční tah
            currentMove = std::make_pair(1, 1);
            //smyčka pro jednotlivé kolo
            while (true) {
                Player &p = players.at(current);
                std::cout << ANSI_CLEAR << std::flush;
                //zobrazení současného tahu
                std::cout << "Current guess: " << currentMove.first << "* " << currentMove.second <<std::endl;
                std::cout << std::endl;
                //zobrazí hráče a jejich kostky, zvýrazní aktuálního hráče
                for (Player &pl: players) {
                    if (pl == p) {
                        std::cout << BLUE << pl.getHidden() << NORMAL << std::endl;
                    } else {
                        std::cout << pl.getHidden() << std::endl;
                    }
                }
                std::cout <<std::endl;
                //hráč provede svůj krok
                auto m = p.makeMove(dices, currentMove);
                //pokud označil bluff nebo pravdu, vyhodnotí se
                if (m.second == 7 || m.second == 8) {
                    std::cout <<ANSI_CLEAR <<std::flush;
                    std::cout << "Current guess: " << currentMove.first << "* " << currentMove.second <<std::endl << std::endl;
                    std::cout << "Players reveal their dices" <<std::endl <<std::endl;
                    for (Player &pl :players) {
                        std::this_thread::sleep_for(std::chrono::seconds(1));
                        if (pl==p) {
                            if (m.second ==7) {
                                std::cout << pl.getRevealed() << " calls " << RED <<"bluff" <<NORMAL <<std::endl;
                            } else {
                                std::cout <<pl.getRevealed() << " calls " <<GREEN <<"believe" <<NORMAL <<std::endl;
                            }
                        }else {
                            std::cout << pl.getRevealed() << std::endl;
                        }
                    }
                    std::cout <<std::endl;
                    //pokud byl odhad správný
                    if (checkDecks()) {
                        //pokud hráč zavolal bluff, pak bude potrestán
                        if (m.second == 7) {
                            punishCurrentPlayer();
                            std::cout << RED << p.getName() << " was WRONG" << NORMAL <<std::endl;
                        } else {
                        //jinak bude potrestán předchozí hráč
                            punishPreviousPlayer();
                            std::cout << GREEN << p.getName() << " was RIGHT" << NORMAL << std::endl;
                        }
                    //pokud byl odhad špatný
                    } else {
                        //pokud byl zavolán bluff, potrestá se předchozí hráč
                        if (m.second == 7) {
                            punishPreviousPlayer();
                            std::cout << GREEN << p.getName() << " was RIGHT" << NORMAL << std::endl;
                        //jinak se potrestá hráč, který odhadl pravdu
                        } else {
                            punishCurrentPlayer();
                            std::cout << RED << p.getName() << " was WRONG" << NORMAL <<std::endl;
                        }
                    }
                    break;
                //hraje další hráč
                } else {
                    currentMove = std::move(m);
                    moveCurrent();
                }
            }
            //po konci kola se vyřadí hráči bez kostek
            //a posune se index začínajícího hráče
            eraseDead();
            moveBeginning();
            dices--;
            std::cout << std::endl << "Press ENTER to continue";
            std::cin.get();
        }
        //vyhlášení vítěze
        std::cout << ANSI_CLEAR <<std::flush;
        std::cout << "Only one player left" <<std::flush;
        std::this_thread::sleep_for(std::chrono::seconds(3));
        std::cout <<ANSI_CLEAR<< "And the winner is" <<std::flush;
        for (int i =0; i<3; i++) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::cout <<'.'<<std::flush;
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout << ANSI_CLEAR<< GREEN<<players.at(0).getName() << NORMAL<<std::flush;
        std::cin.get();
        std::cout << ANSI_CLEAR<<std::flush;
    }
};