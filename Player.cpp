#include <utility>
#include <vector>
#include <string>
#include <iostream>
#include <thread>

#define ROW_CLEAR "\33[2K\r"
#define NORMAL "\x1B[m"
#define REVERSE "\x1B[7m"

class Player {
    private:
        std::vector<int> cup;
        std::string name;
        int dices;
        char strategy;
        bool npc;

        void callBluff(){
            std::cout << name << " calls a bluff" << std::endl;
        }

        void callBelieve() {
            std::cout << name << " believes" <<std::endl;
        }
        void sleep(int milis) {
            std::this_thread::sleep_for(std::chrono::milliseconds(milis));
        }

        /**
         * provede krok, pokud je hráč NPC
         * @param d -počet kostek ve hře
         * @param currentGuess -aktuální tip
         * @return hráčův tip
         */

        std::pair<size_t , size_t> makeNPCMove(const int d, const std::pair<size_t , size_t > currentGuess) {
            std::cout << name << " decides" << std::flush;
            sleep(400);
            for (int i=0; i <3; i++) {
                std::cout << '.' <<std::flush;
                sleep(400);
            }
            std::cout << ROW_CLEAR << std::flush;
            int howManyTimesIhave=0;
            for (int a :cup) {
                if (a == currentGuess.second) {
                    howManyTimesIhave++;
                }
            }

            if (howManyTimesIhave !=0 || currentGuess.first < d/6) {
                std::cout << name << " bets " << currentGuess.first + 1 << "*" << REVERSE<<currentGuess.second << NORMAL << std::endl;
                return std::make_pair(currentGuess.first+1, currentGuess.second);
            }
            if (currentGuess.first >= d/6) {
                if (howManyTimesIhave +2>=currentGuess.first) {
                    callBelieve();
                    return std::make_pair(0,8);
                } else if (howManyTimesIhave -2 <0) {
                    callBluff();
                    return std::make_pair(0, 7);
                }
                if (strategy=='s') {
                    callBluff();
                    return std::make_pair(0, 7);
                } else if (strategy=='b') {
                    callBelieve();
                    return std::make_pair(0,8);
                }
            }
            if (currentGuess.second <6) {
                std::cout << name << " bets 1*" <<REVERSE<<currentGuess.second+1 <<NORMAL<<std::endl;
                return std::make_pair(1,currentGuess.second+1);
            } else {
                if (strategy=='s') {
                    callBluff();
                    return std::make_pair(0, 7);
                } else if (strategy=='b') {
                    callBelieve();
                    return std::make_pair(0,8);
                }
            }
        }

        /**
         * smyčka, ve které uživatel volí svůj krok
         * @param d - počet kostek ve hře
         * @param currentGuess - současný tip ve hře
         * @return hráčův tip
         */
        std::pair<size_t , size_t> makePlayerMove(const int d,const std::pair<size_t , size_t > currentGuess) {
            std::cout << "Controls: \nQ - lower guessed amount, W - increase guessed amount\n A - lower guessed value, S - increase guessed value\n T - believe, L - call bluff, ENTER - confirm\n" <<std::endl;
            //nastavení hráčova odhadu
            std::pair<size_t ,size_t> newGuess;
            if (d> currentGuess.first) {
                newGuess = std::make_pair(currentGuess.first+1,currentGuess.second);
            } else if (d==currentGuess.first && currentGuess.second <6) {
                newGuess = std::make_pair(1, currentGuess.second +1);
            } else {
                newGuess = std::make_pair(0,7);
            }
            char c;
            //reakce na vstupní klávesy
            while (true) {
                std::cout << ROW_CLEAR << std::flush;
                if (newGuess.second==7) {
                  std::cout <<"Your guess: bluff" <<std::flush;
                } else if (newGuess.second==8) {
                    std::cout <<"Your guess: believe" << std::flush;
                } else {
                    std::cout << "Your guess: " << newGuess.first << " *" << REVERSE<<newGuess.second <<NORMAL <<std::flush;
                }
                //přepnutí do raw modu - dovolí číst znaky bez stisknutí enter
                system("stty raw");
                std::cin.get(c);
                if (c=='Q' || c=='q') {
                    if ((newGuess.first >1 && newGuess.second >currentGuess.second) || (newGuess.second==currentGuess.second && currentGuess.first +1 <newGuess.first)) {
                        newGuess.first= newGuess.first -1;
                    }
                    if (newGuess.second ==7 || newGuess.second==8) {
                        newGuess = std::make_pair(d,6);
                    }
                }
                if (c=='W' || c=='w') {
                    if (newGuess.first <d) {
                        newGuess.first = newGuess.first+1;
                    }
                    if (newGuess.second ==7 || newGuess.second==8) {
                        newGuess = std::make_pair(d,6);
                    }
                }
                if (c=='A' || c=='a') {
                    if (newGuess.second> currentGuess.first && newGuess.first> currentGuess.first) {
                        newGuess.second = newGuess.second-1;
                    }
                    if (newGuess.second ==7 || newGuess.second==8) {
                        newGuess = std::make_pair(d,6);
                    }
                }
                if (c=='S' || c=='s') {
                    if (newGuess.second <6) {
                        newGuess.second = newGuess.second+1;
                    }
                    if (newGuess.second ==7 || newGuess.second==8) {
                        newGuess = std::make_pair(d,6);
                    }
                }
                if (c=='L' || c=='l') {
                    newGuess.second = 7;
                }
                if (c=='T' || c=='t') {
                    newGuess.second = 8;
                }
                if (c=='M' || c=='m' || c== '\r' || c== '\n') {
                    if (newGuess.first > currentGuess.first && newGuess.first <=d && newGuess.second <9) {
                        break;
                    } else if (newGuess.second == currentGuess.second && newGuess.first >currentGuess.first && newGuess.first <=d) {
                        break;
                    } else if (newGuess.second >currentGuess.second && newGuess.first <=d && newGuess.first >0) {
                        break;
                    } else if (newGuess.second ==7 || newGuess.second==8) {
                        break;
                    }
                }
                system("stty -raw");
            }
            system("stty -raw");
            return newGuess;
        }
    public:
        Player(std::string playerName, char str, bool n) {
            cup = std::vector<int>();
            name = std::move(playerName);
            dices = 5;
            strategy = str;
            npc = n;
        }
        /**
         * provede hráčův tah
         * @param d současný počet kostek ve hře
         * @param currentGuess tip předchozího hráče
         * @return tip hráče
         */
        std::pair<size_t , size_t> makeMove(const int d, const std::pair<size_t ,size_t> currentGuess) {
            if (npc) {
                auto p= makeNPCMove(d, currentGuess);
                sleep(2500);
                return p;
            } else {
                return makePlayerMove(d,currentGuess);
            }
        }
        /**
         *
         * @return true, pokud má hráč ještě nějaké kostky
         */
        bool canPlay() {
            return dices !=0;
        }

        /**
         * ubere hráčovi jednu kostku
         */
        void decreaseSize() {
            --dices;
        }

        /**
         * provede náhodný hod kostkami
         */
        void throwDices() {
            cup.clear();
            for (int i=0; i <dices; i++) {
                int dice = (std::rand() % 6)+1;
                cup.push_back(dice);
            }
        }
        std::vector<int>& getCup() {
            return cup;
        }
        bool operator == (Player& other) {
            return name == other.name;
        }

        /**
         * pro zobrazení hráče a počtu jeho kostek
         * @return string se jménem hráče a skrytými kostkami
         */
        std::string getHidden() {
            if (npc) {
                std::string revealed = name;
                revealed.append("'s dices: ");
                for (int i :cup) {
                    revealed.push_back('*');
                    revealed.push_back(' ');
                }
                return revealed;
            } else {
                return getRevealed();
            }
        }

        /**
         * pro zobrazení hráče a jeho kostek
         * @return string se jménem hráče a jeho kostkami
         */
        std::string getRevealed() {
            std::string revealed = name;
            revealed.append("'s dices: ");
            for (int i :cup) {
                revealed.append(std::to_string(i));
                revealed.push_back(' ');
            }
            return revealed;
        }
        std::string& getName() {
            return name;
        }
        int getDices() {
            return dices;
        }
};