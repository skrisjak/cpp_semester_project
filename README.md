**Semestrální práce - Liars Dice**

Jako semestrální projekt v předmětu PCC jsem se rozhodl vytvořit konzolovou verzi hry deskové hry Liars Dice. Princip hry je následující
- všichni hráči hodí kostky ve svém pohárku, hráči vidí pouze své kostky
- začínající hráč udělá první odhad, kdy si zvolí kostku a tipne si, kolikrát je celkem na stole
- další hráč smí buďto odhadovaný počet zvýšit, nebo musí zvýšit odhadovanou hodnotu kostky
- další krok, který smí hráč provést, je označit tip předchozího hráče buďto za bluff, nebo mu věřit. V tento moment všichni hráči odhalí své kostky. Pokud hráč tipoval bluff, a tip předchozího hráče byl chybný, pak se předchozímu hráči ubere kostka. Pokud měl ale předchozí hráč pravdu, pak je potrestán hráč, který tip označil za bluff. Stejné je to i naopak.
- hráči opět hodí svými kostkami a začíná další hráč po směru hodinových ručiček. Vítězí hráč, kterému jako jedinému zůstanou kostky na stole

**Ovládání**
Hra čte uživatelský vstup pouze v momentě, kdy je uživatel na řadě. Uživatel mění svůj odhad pomocí kláves, potom jej potvrdí a hra pokračuje dál. 

| Klávesa | Akce   |
| ------  | ------ |
|    Q    |    snížit sumu       |
|    W    |    zvýšit sumu       |
|    A    |    snížit hodnotu    |
|    S    |    zvýšit hodnotu    |
|    L    |    označit bluff     |
|    T    |    označit pravdu    |
|  ENTER  |    potvrdit volbu    |
