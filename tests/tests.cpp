#include "catch.cpp"
#include "../Game.cpp"


/**
     * testuje, jestli npc vrací tip podle pravidel
     * test je pomalý z důvodu thread:sleep, který simuluje lidské rozhodování
 */
TEST_CASE("Player movement") {
        Player player("Player", 's', true);
        player.throwDices();
        for (int i = 0; i < 10; ++i) {
            int guess  = std::rand() %6+1;
            int amount = std::rand() %30+1;
            auto move = std::make_pair(amount, guess);
            auto move2 =player.makeMove(30,move);
            if (move.second ==move2.second) {
                REQUIRE(move2.first > move.first);
                REQUIRE(move2.first >0);
                REQUIRE(move2.first <= 30);
            } else {
                REQUIRE(move2.second > move.second);
                REQUIRE(move2.second <=8);
                REQUIRE(move2.first <=30);
                if (move2.second <=6) {
                    REQUIRE(move2.first >= 1);
                }
            }
        }
}



TEST_CASE("Game-loop") {
    /**
    * otestuje posun hráče a jestli se hráčům uberou kostky
     * jestli se odebere hráč pokud ztratí všechny kostky
    */
    SECTION("Game removes player") {
        Game game(5);
        for (Player &p: game.players) {
            p.throwDices();
        }
        for (int i = 0; i < game.players.size(); i++) {
            game.punishCurrentPlayer();
            game.moveCurrent();
        }
        for (Player &p: game.players) {
            p.throwDices();
        }
        for (Player &p: game.players) {
            REQUIRE(p.getCup().size() == 4);
        }
        size_t i = game.players.size();
        game.eraseDead();
        REQUIRE(game.players.size() == i);
        for (int a = 0; a < 4; a++) {
            game.punishCurrentPlayer();
        }
        game.eraseDead();
        REQUIRE(game.players.size() == i - 1);
    }
    /**
     * otestuje, že hráč ze hry zmizí, pokud se trestá předešlý hráč
     */
    SECTION("Punish previous") {
        Game game(5);
        for (Player &p :game.players) {
            p.throwDices();
        }
        std::string name = game.players.at(game.current).getName();
        for (int i=0; i<5; i++) {
            game.punishPreviousPlayer();
        }
        game.eraseDead();
        auto it =game.players.begin();
        bool found=false;
        while (it != game.players.end()) {
            if (it->getName()==name) {
                found = true;
            }
            it++;
        }
        REQUIRE(found);
    }
        /**
         * otestuje, že hráč ze hry zmizí, pokud se trestá současný hráč
         */
    SECTION("Punish current") {
        Game game(5);
        for (Player &p :game.players) {
            p.throwDices();
        }
        std::string name = game.players.at(game.current).getName();
        for (int i=0; i<5; i++) {
            game.punishCurrentPlayer();
        }
        game.eraseDead();
        auto it =game.players.begin();
        bool found=false;
        while (it != game.players.end()) {
            if (it->getName()==name) {
                found = true;
            }
            it++;
        }
        REQUIRE_FALSE(found);
    }
}